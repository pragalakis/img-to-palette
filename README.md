# img-to-palette

Given an image, extracts the color palette using the median-cut algorithm.

<img src="https://gitlab.com/pragalakis/img-to-palette/raw/master/screenshot.png" />

Screenshot: Viktor's Juric taken from unsplash

# Usage

[![NPM](https://nodei.co/npm/img-to-palette.png)](https://www.npmjs.com/package/img-to-palette)

Takes as an argument an image object `<img>` and returns an array of 8 RGB colors in the form of [[r, g, b], [r, g, b] .....]

### `imgToPalette(image)`

```
let imgToPalette = require('img-to-palette')

let rgbArray = imgToPalette(image)
console.log(rgbArray)  // [[14, 7, 27], [34, 17, 31] ....]
console.log(rgbArray.length)  // 8 

```

## License

MIT
