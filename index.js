// get img data
const getData = image => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  canvas.width = image.width;
  canvas.height = image.height;
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.drawImage(image, 0, 0, canvas.width, canvas.height);

  const data = context.getImageData(0, 0, canvas.width, canvas.height).data;

  const step = 4;
  let arrPixels = [];

  // get rgb colors
  for (let i = 0; i < data.length; i += step) {
    let r = data[i];
    let g = data[i + 1];
    let b = data[i + 2];
    arrPixels.push([r, g, b]);
  }
  return arrPixels;
};

// find the value with the biggest range
const findRange = rgbArr => {
  let minR = Number.MAX_VALUE;
  let minG = Number.MAX_VALUE;
  let maxR = Number.MIN_VALUE;
  let maxG = Number.MIN_VALUE;
  let minB = Number.MAX_VALUE;
  let maxB = Number.MIN_VALUE;

  rgbArr.forEach(v => {
    minR = Math.min(minR, v[0]);
    maxR = Math.max(maxR, v[0]);
    minG = Math.min(minG, v[1]);
    maxG = Math.max(maxG, v[1]);
    minB = Math.min(minB, v[2]);
    maxB = Math.max(maxB, v[2]);
  });

  const rangeR = maxR - minR;
  const rangeG = maxG - minG;
  const rangeB = maxB - minB;
  const biggestRange = Math.max(rangeR, rangeG, rangeB);

  if (biggestRange === rangeR) {
    return 0;
  } else if (biggestRange === rangeG) {
    return 1;
  } else {
    return 2;
  }
};

// median cut algorithm
const quantizer = (rgbArr, minDepth, currentDepth) => {
  if (currentDepth === minDepth) {
    const color = rgbArr.reduce((prev, curr) => {
      prev[0] += curr[0];
      prev[1] += curr[1];
      prev[2] += curr[2];
      return prev;
    });

    color[0] = Math.round(color[0] / rgbArr.length);
    color[1] = Math.round(color[1] / rgbArr.length);
    color[2] = Math.round(color[2] / rgbArr.length);

    return [color];
  }

  const sortBy = findRange(rgbArr);
  rgbArr.sort((a, b) => {
    return a[sortBy] - b[sortBy];
  });

  const mid = rgbArr.length / 2;
  return [
    ...quantizer(rgbArr.slice(0, mid), minDepth, currentDepth - 1),
    ...quantizer(rgbArr.slice(mid + 1), minDepth, currentDepth - 1)
  ];
};

module.exports = function getPalette(image) {
  const arrPixels = getData(image);
  return quantizer(arrPixels, 1, 4);
};
